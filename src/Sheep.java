import com.nttdocomo.ui.Display;
import com.nttdocomo.ui.IApplication;

public class Sheep extends MApplication {

  SheepCanvas canvas;

  public Sheep() {
  }

  public void start() {
    canvas = new SheepCanvas();
    Display.setCurrent(canvas);
    canvas.start();
  }

}
